Summary

Your task is to create backend for a poll webapp using Java and Spring technologies.

The backend should provide a REST endpoint to serve the questionnaire to the frontend.
The backend should also provide another REST endpoint to accept the answers from the frontend and save them in database.
Furthermore the backend should also provide REST endpoint to query the answer statistics of a question.

Make sure you structure code well. However aim to create a working solution first and refactor later if you have time.

Detailed requirements

1. Set up the skeleton of your module using Spring Initializr:

https://start.spring.io/#!type=maven-project&language=java&platformVersion=2.7.8&packaging=jar&jvmVersion=11&groupId=com.example&artifactId=poll-backend&name=poll-backend&description=Demo%20project%20for%20Spring%20Boot&packageName=com.example.poll-backend&dependencies=lombok,web,data-jpa,h2

The created project out-of-the-box:
- will have all dependencies you need to complete your tasks. However you are allowed to add any other Maven dependencies if you feel like.
- will be able to connect to in-memory H2 database. You don't have to set up external database connection. Use Spring Data repositories to access the database.

2. As we are using in-memory H2 database, it is okay that we lose answers upon restart. However make sure that the following sample questionnaire is inserted to the database at startup.

[
{
"question": "Which javascript framework do you like the most?",
"type": "SELECT_ONE",
"answers": ["Angular", "React", "Vue", "Svelte"]
},
{
"question": "Which IDEs do you like to work with?",
"type": "SELECT_MORE",
"answers": ["Webstorm", "IntelliJ IDEA", "VSCode", "Sublime"]
},
{
"question": "What's your proudest moment so far?",
"type": "FREE_TEXT"
}
]

3. The type of one poll question can be:
- SELECT_ONE
- SELECT_MORE
- FREE_TEXT

SELECT_ONE means that the user has to select exactly one answer from the options.
SELECT_MORE means that the user has to select at least one answer from the options.
FREE_TEXT means that a mandatory input area is shown and user has to enter the answer.

4. Design and implement the REST endpoint to serve questionnaire to the frontend. The path of the endpoint should be: /questionnaire.
   Make sure the questions are returned in predictable order.

5. Design and implement the REST endpoint to accept answers from the frontend. All answers of the questionnaire should be submitted in one call. The answers must be persisted in database.
   The path of the endpoint should be: /answers.

6. Design and implement the REST endpoint to query answer count statistics for a given question
   The path of the endpoint should be: /answer-statistics/{questionId}.
   The response format should be exactly as follows:

{
"question": "Which javascript framework do you like the most?",
"type": "SELECT_ONE",
"answers": [
{
"text": "React",
"count": 49
},
{
"text": "Angular",
"count": 35
},
{
"text": "Vue",
"count": 9
},
{
"text": "Svelte",
"count": 0
}
]
}

Make sure the options answers are ordered by answer count.
The endpoint should work for SELECT_ONE and SELECT_MORE questions.

7. If you still have some time left, refactor your code.
- make your code easy to read, apply the clean-code principles
- make sure your code is well-structured into layers as it was part of a large-scale application
- think about possible error scenarios

8. The result should be executable by us. If it requires any special thing to set, please write the instruction into the README.md file.

9. You have 3 hours to complete the tasks. Please push the code to gitlab.com and invite us to the repository (in developer role) when you finished the work or your time is up:
   @tamas.fitos
   @zszepe
   @milan.farkas

10. Reply to this email that you have finished your work.
