package com.example.pollbackend;

import com.example.pollbackend.entity.AnswerOption;
import com.example.pollbackend.entity.Question;
import com.example.pollbackend.entity.QuestionType;
import com.example.pollbackend.entity.Questionnaire;
import com.example.pollbackend.repository.QuestionnaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class QuestionnairePreloader implements ApplicationListener<ApplicationReadyEvent> {

    private final QuestionnaireRepository repository;

    @Autowired
    public QuestionnairePreloader(QuestionnaireRepository repository) {
        this.repository = repository;
    }

    @Override
    public void onApplicationEvent(ApplicationReadyEvent event) {
        preloadQuestions();
    }

    public void preloadQuestions() {
        Questionnaire q = new Questionnaire();
        q.setQuestions(generateQuestions(q));
        repository.save(q);
    }


    private List<Question> generateQuestions(Questionnaire questionnaire) {
        List<Question> questions = List.of(
                Question.builder()
                        .question("Which javascript framework do you like the most?")
                        .questionType(QuestionType.SELECT_ONE)
                        .answerOptions(generateAnswerOptions("Angular", "React", "Vue", "Svelte"))
                        .questionnaire(questionnaire)
                        .build(),
                Question.builder()
                        .question("Which IDEs do you like to work with?")
                        .questionType(QuestionType.SELECT_MULTIPLE)
                        .questionnaire(questionnaire)
                        .answerOptions(generateAnswerOptions("Webstorm", "IntelliJ IDEA", "VSCode", "Sublime"))
                        .build(),
                Question.builder()
                        .question("What's your proudest moment so far?")
                        .questionType(QuestionType.FREE_TEXT)
                        .questionnaire(questionnaire)
                        .build()
        );
        questions.get(0).getAnswerOptions().forEach(a -> a.setQuestion(questions.get(0)));
        questions.get(1).getAnswerOptions().forEach(a -> a.setQuestion(questions.get(1)));
        return questions;
    }

    private List<AnswerOption> generateAnswerOptions(String ...answers) {
        return Arrays.stream(answers)
                .map(this::buildAnswer)
                .collect(Collectors.toList());
    }

    private  AnswerOption buildAnswer(String answer) {
        return AnswerOption.builder()
                .answer(answer)
                .build();
    }
}
