package com.example.pollbackend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PollBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(PollBackendApplication.class, args);
	}

}
