package com.example.pollbackend.entity;

public enum QuestionType {
    SELECT_ONE,
    SELECT_MULTIPLE,
    FREE_TEXT
}
