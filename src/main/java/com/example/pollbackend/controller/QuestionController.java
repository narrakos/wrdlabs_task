package com.example.pollbackend.controller;

import com.example.pollbackend.entity.Answer;
import com.example.pollbackend.entity.AnswerText;
import com.example.pollbackend.entity.Question;
import com.example.pollbackend.entity.QuestionType;
import com.example.pollbackend.repository.AnswerRepository;
import com.example.pollbackend.repository.QuestionRepository;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
@RequiredArgsConstructor
public class QuestionController {
    private final QuestionRepository questionRepository;

    @GetMapping("/answer-statistics/{questionId}")
    public ResponseEntity<QuestionStatisticsAnswer> getStatistics(@PathVariable Long questionId) {
        Question question = questionRepository.findById(questionId).orElseThrow();
        return ResponseEntity.ok(new QuestionStatisticsAnswer(question));
    }

    private class QuestionStatisticsAnswer {
        private String question;
        private QuestionType questionType;

        private List<AnswerStatistic> answers;

        public QuestionStatisticsAnswer(Question question) {
            this.question = question.getQuestion();
            this.questionType = question.getQuestionType();
            Map<String, Long> answers = question.getAnswers().stream()
                    .map(Answer::getAnswers)
                    .flatMap(List::stream)
                    .map(AnswerText::getAnswerText)
                    .collect(Collectors.groupingBy(Function.identity(), Collectors.counting()));
            this.answers = answers.entrySet().stream()
                    .map(entry -> new AnswerStatistic(entry.getKey(), entry.getValue()))
                    .collect(Collectors.toList());
        }

        @AllArgsConstructor
        private class AnswerStatistic {
            private String text;
            private long statistics;
        }
    }
}
