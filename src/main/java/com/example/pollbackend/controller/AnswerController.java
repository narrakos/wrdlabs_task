package com.example.pollbackend.controller;

import com.example.pollbackend.entity.Answer;
import com.example.pollbackend.entity.AnswerText;
import com.example.pollbackend.entity.Question;
import com.example.pollbackend.entity.QuestionType;
import com.example.pollbackend.repository.AnswerRepository;
import com.example.pollbackend.repository.QuestionRepository;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
public class AnswerController {

    private final AnswerRepository answerRepository;
    private final QuestionRepository questionRepository;

    @PostMapping("answers")
    public ResponseEntity<Object> submitAnswer(AnswerRequest answerRequest) {
        String question = answerRequest.getQuestion();
        Question savedQuestion = questionRepository.findByQuestion(question).orElseThrow();
        Answer answer = Answer.builder()
                .answers(buildAnswerTexts(answerRequest.getAnswers()))
                .question(savedQuestion)
                .build();
        answer.getAnswers().forEach(at -> at.setAnswer(answer));
        answerRepository.save(answer);
        return ResponseEntity.ok().build();
    }

    private List<AnswerText> buildAnswerTexts(List<String> answers) {
        return answers.stream().map(a -> AnswerText.builder().answerText(a).build()).collect(Collectors.toList());
    }


    @AllArgsConstructor
    @Data
    private class AnswerRequest {
        private String question;
        private QuestionType questionType;
        private List<String> answers;
    }
}
