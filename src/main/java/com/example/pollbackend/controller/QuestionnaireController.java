package com.example.pollbackend.controller;

import com.example.pollbackend.entity.Questionnaire;
import com.example.pollbackend.repository.QuestionnaireRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QuestionnaireController {

    private final QuestionnaireRepository repository;

    @Autowired
    public QuestionnaireController(QuestionnaireRepository repository) {
        this.repository = repository;
    }

    @GetMapping("/questionnaire")
    public ResponseEntity<Questionnaire> getQuestionnaire() {
        //TODO format data
        Questionnaire questionnaire = repository.findById(1L).orElseThrow();
        return ResponseEntity.ok(questionnaire);
    }


}
